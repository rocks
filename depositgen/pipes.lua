-- experimental pipe generator
local l=depositgen.l
local print=l.print

-- the public table of registered pipes
depositgen.pipes={}
local regs=depositgen.pipes

depositgen.register_pipe= function(descr)
 local pipe={
  ymin=(descr.ymin or -10000),
  ymax=(descr.ymax or 200),
  scarcity=descr.scarcity,
  width=descr.width-1,
  content={ name=descr.content },
  scatter={}
 }
 table.insert(depositgen.l.ToResolve,pipe.content)
 for _,sc in pairs(descr.scatter) do
  local psc={
   scarcity=sc.scarcity,
   size=(sc.size or pipe.width),
   density=sc.density,
   content={ name=sc.content }
  }
  table.insert(depositgen.l.ToResolve,psc.content)
  table.insert(pipe.scatter,psc)
 end
 return table.insert(regs,pipe)
end

do
local sample_pipe_def={
 ymin=-200, ymax=-6,
 scarcity=80,
 width=4,
 content="default:glass",
 scatter={
  { scarcity=7, density=4, content="default:mese", size=2 }
 }
}
--depositgen.register_pipe(sample_pipe_def)
end

depositgen.l.ToResolve.air={name="air"}

local function generate(mgc)
 local t1 = os.clock()
 
 local data = mgc.data
 local area = mgc.area
 local pr=mgc.pr
 local minp,maxp=mgc.minp,mgc.maxp
 local chunksizer = maxp.x - minp.x + 1
 local chunksize = chunksizer + 1
 local pmapsize = {x = chunksize, y = chunksize, z = chunksize}
 local minpxz = {x = minp.x, y = minp.z}

 for _,descr in pairs(regs) do
  local numpipes_raw=(chunksize/descr.scarcity)
  local numpipes = math.floor(numpipes_raw + (pr:next(0,99)/100))

  for vc=1, numpipes do
   local pointA=l.rndvector(mgc)
   local pointB=l.rndvector(mgc)
   local pointC=l.rndvector(mgc)
   local step=(1.8)/(vector.distance(pointA,pointB)+vector.distance(pointB,pointC))

   for t=0, 1, step do
    local p=vector.multiply(pointA,(1-t)^2)
    local di
    p=vector.add(p, vector.multiply(pointB,2*t*(1-t)) )
    p=vector.add(p, vector.multiply(pointC,t*t) )
    p=vector.round(p)
    local radiusp=math.floor(descr.width/2)
    local radiusn=-descr.width+radiusp
    --<scatter>
    for _,ore in pairs(descr.scatter) do
     if pr:next(0,ore.scarcity)==0 then
      local ocx=pr:next(radiusn,radiusp)
      local ocy=pr:next(radiusn,radiusp)
      local ocz=pr:next(radiusn,radiusp)
      for y=ocy-ore.size, ocy+ore.size do
      for x=ocx-ore.size, ocx+ore.size do
      for z=ocz-ore.size, ocz+ore.size do
       if pr:next(0,ore.density)==0 then
        di=area:index(p.x+x,p.y+y,p.z+z)
        if data[di]==depositgen.l.ToResolve.air.id then
         data[di]=ore.content.id
        end
       end
      end end end
     end
    end
    --</scatter>
    --<brush>
    for y= radiusn, radiusp do
    for x= radiusn, radiusp do
    for z= radiusn, radiusp do
     di=area:index(p.x+x,p.y+y,p.z+z)
     if data[di]==depositgen.l.ToResolve.air.id then
      data[di]=descr.content.id
     end
    end end end
    --</brush>
    --brush(data,area,p,descr.radius,content,descr.scatter,orepr)
   end
  end

 end --</apipe>

 l.print("pipes "..math.ceil((os.clock() - t1) * 1000).." ms ")
end

table.insert(depositgen.l.OnGenerate,generate)
