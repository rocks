depositgen={}
local l={}
depositgen.l=l

l.print=function(text)
 minetest.log("action","[depositgen] "..text)
end

l.print("mod initializing")

local modpath=minetest.get_modpath(minetest.get_current_modname())

l.OnGenerate={}
l.OnInit={}
l.ToResolve={}

local includes={
"utils","pipes","veins"
}

minetest.register_on_generated(function(minp,maxp,seed)
 l.print("on generated")
 local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
 local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
 local pr=PseudoRandom(seed)
 local data=vm:get_data()
 local mgc={minp=minp,maxp=maxp,pr=pr,data=data,area=area}
 for i=1, #l.OnGenerate do
  l.OnGenerate[i](mgc)
 end
 vm:set_data(data)
 vm:write_to_map()
end)

minetest.register_on_mapgen_init(function(mapgen_params)
 -- todo: disable caves and ores
 -- todo: sort layers
 --Resolve all mapgen nodes to content ids
 l.print("Resolving node names")
 for index,i in pairs(l.ToResolve) do
  i.id=minetest.get_content_id(i.name)
  i.name=nil --free some memory
 end
 l.print("running mapgen initialization tasks")
 for _,i in pairs(l.OnInit) do
  i(mapgen_params)
 end
end)

for _,s in pairs(includes) do dofile(modpath.."/"..s..".lua") end
