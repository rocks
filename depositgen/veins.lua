-- experimental fast vein generator

local l=depositgen.l
local print=l.print

-- the public table of registered veins
depositgen.veins={}
local regs=depositgen.veins

depositgen.register_vein= function(descr)
 local vein={
  ymin=(descr.ymin or -10000),
  ymax=(descr.ymax or 200),
  scarcity=descr.scarcity,
  content={ name=descr.content },
  scatter={}
 }
 table.insert(depositgen.l.ToResolve,vein.content)
 for _,sc in pairs(descr.scatter) do
  local psc={
   scarcity=sc.scarcity,
   size=(sc.size or vein.width),
   density=sc.density,
   content={ name=sc.content }
  }
  table.insert(depositgen.l.ToResolve,psc.content)
  table.insert(vein.scatter,psc)
 end
 return table.insert(regs,vein)
end

do
local sample_vein_def={
 ymin=-200, ymax=-6,
 scarcity=80,
 content="default:dirt",
 scatter={
  { scarcity=7, density=4, content="default:mese", size=2 }
 }
}
depositgen.register_vein(sample_vein_def)
end

local function generate(mgc)
 local t1 = os.clock()
 local data = mgc.data
 local area = mgc.area
 local pr=mgc.pr

 local chunksizer = mgc.maxp.x - mgc.minp.x + 1
 local chunksize = chunksizer + 1
 local pmapsize = {x = chunksize, y = chunksize, z = chunksize}
 local minpxz = {x = mgc.minp.x, y = mgc.minp.z}
 
 for _,descr in pairs(regs) do
  local num = math.floor( (chunksize/descr.scarcity) + (pr:next(0,99)/100) )
  for vc=1, num do

  local A=l.rndvector(mgc)
  local B=l.rndvector(mgc)
  local C=l.rndvector(mgc)
  local D=l.rndvector(mgc)
  local l1=vector.distance(A,C)+vector.distance(C,B)
  local l2=vector.distance(A,D)+vector.distance(D,B)
  local step=2/math.max(l1,l2)
  for t=0, 1, step do
   local P=vector.multiply(A,(1-t)^2)
   P=vector.add(P, vector.multiply(B,t*t) )
   local Q=vector.add(P, vector.multiply(D,2*t*(1-t)) )
   P=vector.add(P, vector.multiply(C,2*t*(1-t)) )
   local step2=1/vector.distance(P,Q)
   for u=0, 1, step2 do
    local R=vector.add(vector.multiply(P,(1-u)), vector.multiply(Q,u) )
    --<brush>
    local di=area:indexp(vector.round(R))
    if data[di] then
     data[di]=descr.content.id
    end
    --</brush>
   end
  end
  --<ores>
  local cluster=descr.scatter[1]
  for xxx=1, 5 do
   local t=pr:next(0,100)/100
   local u=pr:next(0,100)/100
   local P=vector.multiply(A,(1-t)^2)
   P=vector.add(P, vector.multiply(B,t*t) )
   local Q=vector.add(P, vector.multiply(D,2*t*(1-t)) )
   P=vector.add(P, vector.multiply(C,2*t*(1-t)) )
   local R=vector.add(vector.multiply(P,(1-u)), vector.multiply(Q,u) )
   l.SpawnCluster(mgc,vector.round(R),l.ToResolve.air.id,cluster.size,cluster.content.id,cluster.density)
  end
  --</ores>
  end
 end

 print("vein "..math.ceil((os.clock() - t1) * 1000).." ms ")
end

table.insert(depositgen.l.OnGenerate,generate)
