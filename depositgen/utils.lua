function depositgen.l.SpawnCluster(mgc,pos,wherein,size,content,density)
 for y=pos.y-size, pos.y+size do
 for x=pos.x-size, pos.x+size do
 for z=pos.z-size, pos.z+size do
  if mgc.pr:next(0,density)==0 then
   di=mgc.area:index(x,y,z)
   if mgc.data[di]==wherein then
     mgc.data[di]=content
   end
  end
 end end end
end
function depositgen.l.rndvector(mgc)
 local chunksizer = mgc.maxp.x - mgc.minp.x + 1
 return vector.new(mgc.pr:next(0,chunksizer)+mgc.minp.x,mgc.pr:next(0,chunksizer)+mgc.minp.y,mgc.pr:next(0,chunksizer)+mgc.minp.z)
end